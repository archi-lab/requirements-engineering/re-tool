---
acronym: validationThroughPrototyping
type: review
isTemplate: true
author: 
    - ili
artefact: [functionalRequirement, checkForPlagiarism] 
conducting:
    date: 2024-05-23
    timeFrom: "16:00"
    timeUntil: "18:00"
    location: Gummersbach
reviewer: One reviewer of the stakeholder role Lecturer took part in the review.
reviewType: Commenting
method: 
    name: Prototyping
    description: > 
        A non-functional prototype was developed to illustrate the requirement. The prototype was shown to the reviewer.  
additionalDocuments:
    - purpose: Prototype
      url: http://figma.com
stakeholderRoles: 
    - student
    - lecturer
history:
    v1:
        date: 2024-05-26
        comment: Initially created
ignore: 

---

## Issues identified

1. TBD
2. TBD



